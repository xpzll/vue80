import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false


// ---------- vant 导入部分 --------------------
import { Tabbar, TabbarItem } from 'vant';
import { NavBar } from 'vant';
import { Col, Row } from 'vant';
import { Image as VanImage } from 'vant';
import { Cell, CellGroup } from 'vant';
import { Icon } from 'vant';
import { Search } from 'vant';

Vue.use(Search);
Vue.use(Icon);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(VanImage);
Vue.use(Col);
Vue.use(Row)
Vue.use(NavBar);
Vue.use(Tabbar);
Vue.use(TabbarItem);



// 导入路由
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// 导入组件
// @符号在vue_cli创建的项目里代表src目录
// 文件夹后面如果没跟文件名，那么就默认找index.js或者index.vue
import Layout from '@/views/Layout'
import Play from '@/views/Play'
import Home from '@/views/Home'
import Seach from '@/views/Search'

// 创建路由规则
const routes = [
  { path: '', redirect: '/layout/home' },
  {
    name: 'layout',
    path: '/layout',
    component: Layout,
    children: [
      {
        name: 'home',
        path: 'home',
        component: Home,
        meta: {
          title: '首页'
        }
      },
      {
        name: 'search',
        path: 'search',
        component: Seach,
        meta: {
          title: '搜索'
        }
      },
    ]
  },
  { name: 'play', path: '/play', component: Play },
]

// 创建路由对象
const router = new VueRouter({
  routes
})


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
