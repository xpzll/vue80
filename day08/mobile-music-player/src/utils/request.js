// 本文件就是专门提供发请求的对象
import axios from 'axios'

// 以后的axios的基地址永远都只有这一个
// axios.defaults.baseURL = 'http://music.zllhyy.cn/'

// 要想办法，能设置多个基地址（简称为多基地址）
// 我们要明白，我们此时只有一个 axios 对象，所以意味着，你设置基地址就只能设置一个

// 那我要是克隆出多个axios对象？？就可以设置多个基地址了
// 用不同的axios对象，发不同服务器上的接口

// 克隆出axios对象，并设置了克隆对象的基地址
// 那么以后这个request就是专门发这个基地址的axios对象
let request = axios.create({
    baseURL: 'https://autumnfish.cn/'
})



// 因为我们这个项目里就只有一个基地址，所以写上面一句就够了
// 然后暴露出去，因为我们现在只有一个，所以默认暴露即可
// 但是如果真的有多个，那么就要用 按需暴露，一个一个暴露
export default request