// 代表本文件专门封装发首页请求的接口
// 导入request文件里的对象即可
import request from '@/utils/request'


// 需要封装一个用来获取推荐歌单的接口，并暴露出去，用按需暴露
// 因为我们到时候会封装多个接口函数，所以用按需暴露

export const recommand = (params) => {
    // 这里能写.then，是因为这里得到的是一个promise对象
    // 但是我们在这里又不能写死.then，所以干脆把这个promise对象返回出去
    // 调用者想怎么写.then就怎么写.then
    return request({
        url: '/personalized',
        // 是有参数的，
        // 因为名字都叫params，根据语法是可以简写的
        // params: params
        params
    })


    // .then ( res => {

    //     // 拿到结果，对结果做处理
    //     // 如果在这里写.then，就相当于对结果的处理写死了
    //     // 那么不管哪个页面调用，拿到接口返回数据都只能做这里写死的操作
    //     // 那这样就失去了封装的意义
    // })
}


// 封装一个获取最新音乐的接口，并暴露出去
export const newMusic = (params) => {

    return request({
        url: '/personalized/newsong',
        params
    })
}