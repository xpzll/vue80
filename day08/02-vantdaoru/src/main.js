import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false


// 导入button，并注册成全局
import { Button } from 'vant';
Vue.use(Button);
import { Rate } from 'vant';
Vue.use(Rate);
import { Switch } from 'vant';
Vue.use(Switch);

new Vue({
  render: h => h(App),
}).$mount('#app')
