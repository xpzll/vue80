import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false


// 1. 导入路由
import VueRouter from 'vue-router'
// 2. 安装路由
Vue.use(VueRouter)

// 3.导入组件
import discover from './views/discover'
import my from './views/my'
import friend from './views/friend'
// 导入404页面
import notFound from './views/notFound'

// 先导入子路由的组件
import toplist from './views/toplist'
import songList from './views/songList'
import radio from './views/radio'

// 4. 定义路由规则
// 这里的名字必须叫routes
const routes = [
  // 路由重定向
  // 当用户在路径上毛都没输入时，那么会自动转到/discover
  // 也可以写 / 也可以就写空字符串，都是代表什么都不输入就跳转到/discover
  { path: '', redirect: '/discover' },
  // path:路径 component：组件
  // 注意：这里的路径是指在网页上访问的路径，不要写组件文件的路径
  // 注意：目前路径都要加一个 `/`
  { 
    path: '/discover', 
    component: discover,
    // 写子路由的地方
    children: [
      // 规则跟之前写的是一样的，也可以有name、path、componet属性\
      // 子路由的路径可以不用加/
      // 如果加了/就代表直接根目录访问这个路径，不加/，就代表路径上要先有父路由的路径再加自己的路径
      // 例如这里的就是： /discover/toplist
      // 建议大家写路由的时候都加name属性，但是目前就不加了，因为在学习其他知识
      // 为什么这么建议？因为加了name属性可以方便路由传值
      { path: 'toplist', component: toplist },
      { path: 'songlist', component: songList },
      { path: 'radio', component: radio },
    ] 
  },
  // 这时候路径上有 :不是代表你访问时路径要加冒号，而是代表这是一个要传递的参数
  // 所以/my/:id 意思是你要传值，传法类似于： /my/101
  { path: '/my/:id/:xx', component: my },
  { name: 'fr', path: '/friend', component: friend },
  // 其实就是一条规则
  // 就是除了上面的路径外，用户输入任意路径都访问到404的组件
  { path: '*', component: notFound }
]

// 5. 创建 router 实例，然后传 `routes` 配置
// 你还可以传别的配置参数, 不过先这么简单着吧。
const router = new VueRouter({
  routes,
  // mode: 'history'
})


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
