// 导入Vue
import Vue from 'vue'
// 导入主组件，也就是当前文件夹里的App.vue
import App from './App.vue'

// 无视，要不要多打印一个提示
Vue.config.productionTip = false


// 要使用要先实例化一个Vue对象
// 所以这里就是在实例化
new Vue({
  
  // render渲染函数，作用把某个组件渲染到被Vue管理的容器上
  // 具体一点就是把App.vue这个组件渲染到id为app的div上去了
  // 这里是替换渲染
  render: h => h(App),

}).$mount('#app') 
// $mount('#app')相当于就是  el:#app，
// 所以这句话的意思是找到 `public/index.html` 里的id为app的div，交给Vue去管理
