import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false

// 导入全局样式
import './styles/base.css'
import './styles/index.css'

new Vue({
  // 把App里的template交给渲染函数去渲染
  render: h => h(App),
}).$mount('#app')
