import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false

// 把bug挂载到vue的原型上
// 规范：把Vue原型上的东西前面都加一个$符号，方便区分这到底是组件自己的数据还是原型上的数据
Vue.prototype.$bus = new Vue()


// Vue.prototype.$msg ='hello'

new Vue({
  render: h => h(App),
}).$mount('#app')
