import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 全局的都是写在main.js


Vue.directive('color', {

  // el（参数1）：使用这个指令的dom对象
  // bingding(参数2)：
  //    就是指令的信息，里面的指令的名字有指令的完整写法
  //    最主要的是里面有个value属性，是这个指令绑定的值
  // inserted：是指当绑定指令的标签插入到dom时调用一次
  inserted(el, binding) {

    console.log('inserted调用了')
    el.style.color = binding.value
  },

  // update：会在这个指令绑定的数据发生改变时调用
  update(el, binding) {

    // console.log('update调用了',el, binding)
    // 把改变后的值重新赋值给dom元素
    el.style.color = binding.value
  }

})

new Vue({
  render: h => h(App),
}).$mount('#app')
