import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false

// 导入组件
import panel from './components/panel'

// 技巧：注册Vue的全局东西时，一般单词都不要加s
// 一般局部的有s，例如注册全局组件时就叫component而局部组件时叫components
// 参数1：组件名
// 参数2：对应的组件
Vue.component('panel', panel)


// 全局过滤器
// 参数1：过滤器名字
// 参数2：这个过滤器对应的函数
Vue.filter('formatTime', (val) => {
  // 参数就是原材料，然后你返回什么，过滤器的结果就是什么
  return '我是全局过滤器处理的时间'
})

new Vue({
  render: h => h(App),
}).$mount('#app')
